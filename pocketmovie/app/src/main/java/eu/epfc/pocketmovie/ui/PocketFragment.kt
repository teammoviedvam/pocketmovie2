package eu.epfc.pocketmovie.ui




import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import eu.epfc.pocketmovie.service.MovieListAdapter
import eu.epfc.pocketmovie.R
import eu.epfc.pocketmovie.service.DataRepository
import eu.epfc.pocketmovie.model.Movie

/**
 * A simple [Fragment] subclass.
 */
class PocketFragment : Fragment(), MovieListAdapter.AdapterListerner{


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pocket, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)
        super.onViewCreated(view, savedInstanceState)

        val recyclerView:RecyclerView = view.findViewById(R.id.recyclerView)
        val adapter = MovieListAdapter(this)

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter

        // Create the observer which updates the UI.
        val moviesObserver = Observer<List<Movie>> { listMovie ->
            adapter.submitList(listMovie)
        }

        context?.let {
            val repository = DataRepository.getInstance(it.applicationContext)
            repository.getMoviesInPocket().observe(viewLifecycleOwner,moviesObserver)
        }


    }

    override fun onNextPage() {
        // no implementation for database
    }


    override fun onListItemClick(movie: Movie) {
        val detailIntent = Intent(context, DetailActivity::class.java)
        detailIntent.putExtra(DetailActivity.MOVIE_ID, movie.id)
        startActivity(detailIntent)
    }

}



