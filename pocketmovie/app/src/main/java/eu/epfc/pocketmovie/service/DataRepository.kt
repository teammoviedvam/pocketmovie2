package eu.epfc.pocketmovie.service


import android.content.Context
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import eu.epfc.pocketmovie.db.*
import eu.epfc.pocketmovie.model.Movie
import eu.epfc.pocketmovie.model.MovieDetail
import eu.epfc.pocketmovie.model.VideoDetail
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.Executors


class DataRepository {
    companion object{
        private var sInstance: DataRepository? = null

        fun getInstance(context: Context): DataRepository {
            val instance =
                sInstance
            if (instance != null) {
                return instance
            }
            val newInstance = DataRepository(context)
            sInstance = newInstance
            return newInstance
        }
    }

    private constructor(context: Context){
        this.context = context.applicationContext
        pocketDAO = MovieDataBase.getInstance(
            context.applicationContext
        ).pocketDAO()
        webService= TMDbService.retrofit.create(
            TMDbService::class.java)
    }
    private val pocketDAO: PocketDAO
    private val webService: TMDbService
    private val context:Context


    val listMovie    = MutableLiveData<TMDbService.ListResult<Movie>>()
    val movieDetail  = MutableLiveData<MovieDetail>()
    val videoDetail  = MutableLiveData<TMDbService.ListResult<VideoDetail>>()

    /**
     *  TMDb Provider
     *  1) list movies by page
     *  2) Detail of a movie by MovieId
     *  3) Video detail for a movie by movieId
     */
    fun getTMDbMovies(pageID:Int) {
        makeCall<TMDbService.ListResult<Movie>>(webService.getPopularMoviesByPage(pageID),listMovie)
    }

    fun getMovieDetail(movieID:Long) {
        makeCall<MovieDetail>(webService.getMovieDetailById(movieID),movieDetail)
    }

    fun getVideoDetail( movieID:Long){
        makeCall<TMDbService.ListResult<VideoDetail>>(webService.getVideoDetailById(movieID),videoDetail)
    }

    fun <T>makeCall(call: Call<T>,liveData: MutableLiveData<T>){
        call.enqueue(object:Callback<T>{
            override fun onFailure(call: Call<T>, t: Throwable) {
                Toast.makeText(context,"Connexion internet impossible",Toast.LENGTH_LONG).show()
            }
            override fun onResponse(call: Call<T>, response: Response<T>) {
                if (response.isSuccessful) {
                    liveData.value = response.body()
                } else {
                    Toast.makeText(context,"Erreur du serveur TMDb",Toast.LENGTH_LONG).show()
                }
            }
        } )
    }


    /**
     * Pocket provider
     * 1) list of movie in the pocket
     * 2) Add a movie in the pocket by movieId
     * 3) Remove a movie from the pocket by movieId
     */
    fun getMoviesInPocket():LiveData<List<Movie>>{
        return pocketDAO.getMoviesInPocket()
    }

    fun addMovieInPocket(movie: Movie){
        Executors.newSingleThreadExecutor().execute{
            pocketDAO.addMovieInPocket(movie)
        }
    }

    fun removeMovieOfPocket(movieId:Long){
        Executors.newSingleThreadExecutor().execute{
            pocketDAO.removeMovieFromPocket(movieId)
        }
    }

}











