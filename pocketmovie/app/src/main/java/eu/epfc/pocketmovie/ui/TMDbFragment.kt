package eu.epfc.pocketmovie.ui



import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import eu.epfc.pocketmovie.service.MovieListAdapter
import eu.epfc.pocketmovie.R
import eu.epfc.pocketmovie.service.DataRepository
import eu.epfc.pocketmovie.model.Movie



/**
 * A simple [Fragment] subclass.
 */
class TMDbFragment : Fragment(),
    MovieListAdapter.AdapterListerner {

    private var currentPage:Int=0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tmdb, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)

        val recyclerView:RecyclerView = view.findViewById(R.id.recyclerView)
        val adapter = MovieListAdapter(this)

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter

        // Create the observer which updates the UI.
        val moviesObserver = Observer<List<Movie>> { listMovie ->
            val newList:List<Movie> = adapter.currentList.union(listMovie).toList()
            adapter.submitList(newList)
            // when a new page arrive we incremental the counter's page
            currentPage++
        }


        context?.let{
            val repository = DataRepository.getInstance(it.applicationContext)
            repository.listMovie.observe(viewLifecycleOwner,moviesObserver )
            //we ask a new page, this is the next page !!!
            repository.getTMDbMovies( currentPage+1)
        }
    }

    override fun onNextPage() {
        context?.let{
            //we ask a new page, this is the next page !!!
            DataRepository.getInstance(it.applicationContext).getTMDbMovies( currentPage+1)
        }
    }

    override fun onListItemClick(movie: Movie) {
        val detailIntent = Intent(context, DetailActivity::class.java)
        detailIntent.putExtra(DetailActivity.MOVIE_ID, movie.id)
        startActivity(detailIntent)
    }


}
