package eu.epfc.pocketmovie.service

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import eu.epfc.pocketmovie.R
import eu.epfc.pocketmovie.model.Movie
import eu.epfc.pocketmovie.ui.getUrlPoster


class MovieListAdapter(val adapterListerner: AdapterListerner) : ListAdapter<Movie, MovieListAdapter.MovieViewHolder>(
    DiffCallback()
) {

    private class DiffCallback: DiffUtil.ItemCallback<Movie>() {
        override fun areItemsTheSame(oldItem: Movie, newItem: Movie) = oldItem.id == newItem.id
        override fun areContentsTheSame(oldItem: Movie, newItem: Movie)= oldItem.id == newItem.id
    }


    /**
     * This interface is the message (for activity or else) for ask the display of a movie detail
     */
    interface AdapterListerner : ListItemClickListener,
        NavigationListener

    /**
     * This interface is the message (for activity or else) for ask the display of a movie detail
     */
    interface ListItemClickListener {
        fun onListItemClick(movie: Movie)
    }

    /**
     * This interface is the message (for activity or else) for ask new page
     */
    interface NavigationListener {
        fun onNextPage()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return MovieViewHolder(layoutInflater.inflate(R.layout.item_movie, parent, false))
    }


    /**
     * We return the numbers of movies
     */
    override fun getItemCount(): Int {
        return currentList.size
    }

    override fun getItem(position: Int): Movie {
        if(position== currentList.size-1 ){
            adapterListerner.onNextPage()
        }
        return super.getItem(position)
    }


    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
                val movie = getItem(position)
                holder.ratingTextView.text = "rating : ${movie.vote_average}"
                holder.titleTextView.text = "title : ${movie.title}"
                Picasso.get().load(getUrlPoster(movie.poster_path))
                    .into(holder.posterImageView)
    }

    inner class MovieViewHolder: RecyclerView.ViewHolder{

        val titleTextView : TextView
        val ratingTextView : TextView
        val posterImageView:ImageView
        val itemViewGroup:ViewGroup

        constructor(itemView: View) : super(itemView){
            itemViewGroup = itemView as ViewGroup
            titleTextView   = itemViewGroup.findViewById(R.id.textTitle)
            ratingTextView  = itemViewGroup.findViewById(R.id.textRating)
            posterImageView = itemViewGroup.findViewById(R.id.imagePoster)
            itemView.setOnClickListener({adapterListerner.onListItemClick(getItem(adapterPosition))})
        }

    }



}