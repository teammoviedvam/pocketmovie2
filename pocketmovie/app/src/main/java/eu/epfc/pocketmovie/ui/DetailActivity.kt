package eu.epfc.pocketmovie.ui

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.squareup.picasso.Picasso
import eu.epfc.pocketmovie.R
import eu.epfc.pocketmovie.model.Movie
import eu.epfc.pocketmovie.model.MovieDetail
import eu.epfc.pocketmovie.model.VideoDetail
import eu.epfc.pocketmovie.service.DataRepository
import java.lang.Exception



class DetailActivity : AppCompatActivity() {

    companion object{
        const val MOVIE_ID = "MOVIE_ID"
    }

    private var isInThePocket: Boolean = false
    private var movieID: Long = 0
    private var videoDetail: VideoDetail? = null
    private var wrong = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        if (savedInstanceState == null) {
            // for a new instance
            movieID = intent.getLongExtra(MOVIE_ID, 0)
        } else {
            // for a old instance
            movieID = savedInstanceState.getLong(MOVIE_ID)
        }

        val movieObserver = Observer<MovieDetail> { fetchTheMovie(it) }
        val videoObserver = Observer<List<VideoDetail>> { activeVideoButton(it) }
        val pocketObserver = Observer<List<Movie>> { inThePocket(it) }

        val repository = DataRepository.getInstance(this.applicationContext)

        repository.movieDetail.observe(this, movieObserver)
        repository.videoDetail.observe(this, videoObserver)
        repository.getMoviesInPocket().observe(this, pocketObserver)

        repository.getMovieDetail(movieID)
        repository.getVideoDetail(movieID)
    }


    /**
     * receive the detail of a movie
     *  is it the good movie? -> Wrong= true/false
     */

    private fun fetchTheMovie(movieDetail: MovieDetail) {

        //fix bug
        if(movieDetail.id == this.movieID ) {
           val titleTextView = findViewById<TextView>(R.id.textTitle)
            val ratingTextView = findViewById<TextView>(R.id.textRating)
            val dateTextView = findViewById<TextView>(R.id.textDate)
            val overviewTextView = findViewById<TextView>(R.id.textOverview)
            val posterImageView = findViewById<ImageView>(R.id.imagePoster)
            val pocketCheckBox = findViewById<CheckBox>(R.id.checkPocket)
            val videoButton = findViewById<Button>(R.id.buttonVideo)

            titleTextView.text = movieDetail.title
            ratingTextView.text = "rating : ${movieDetail.vote_average.toString()}"
            dateTextView.text = movieDetail.releaseDate
            overviewTextView.text = movieDetail.overview
            Picasso.get().load(getUrlPoster(movieDetail.poster_path)).into(posterImageView)
            pocketCheckBox.setOnClickListener({ swapMovieInPocket(movieDetail) })
            videoButton.setOnClickListener({ playVideo() })

            wrong = false

        } else {
            wrong = true
        }
    }

    /**
     * manage the video
     * 1) activeVideoButton:
     *      if this is the good movie
     *      if this movie have a video
     * 2) playVideo
     */
    private fun activeVideoButton(listVideoDetail: List<VideoDetail>) {
        val button = findViewById<Button>(R.id.buttonVideo)
        button.visibility = View.INVISIBLE

        if (!wrong) {
            if (listVideoDetail.size > 0) {

                button.visibility = View.VISIBLE
                this.videoDetail = listVideoDetail[0]
            }
        }
    }


    private fun playVideo() {

        videoDetail?.let {
            val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:${it.key}"))
            val webIntent =
                Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=${it.key}"))
            try {
                startActivity(appIntent)
            } catch (e: Exception) {
                startActivity(webIntent)
            }
        }

    }

    /**
     * Manage the pocket
     * 1) this movie is in the pocket? -> inThePocket
     * 2) invert the pocket ->  swapMovieInPocket
     * 3.a) addMovieInPocket
     * 3.b) removeMovieInPocket
     */

    private fun inThePocket(moviesPocket: List<Movie>) {
        val result = moviesPocket.filter { it.id == movieID }
        isInThePocket = (result.size > 0)

        val pocketCheckBox = findViewById<CheckBox>(R.id.checkPocket)
        pocketCheckBox.isChecked = isInThePocket
    }

    private fun swapMovieInPocket(movie: MovieDetail) {
        if (isInThePocket) { // le film y est on retire
            removeMovieInPocket(movie.id)
        } else { //sinon l'inverse: on l'y met
            addMovieInPocket(movie)
        }
        isInThePocket = isInThePocket.not()
    }


    private fun addMovieInPocket(detail: MovieDetail) {
        val movie = Movie(
            detail.id,
            detail.title,
            detail.vote_average,
            detail.poster_path
        )
        val repository = DataRepository.getInstance(this)
        repository.addMovieInPocket(movie)
    }

    private fun removeMovieInPocket(movieId: Long) {
        val repository = DataRepository.getInstance(this)
        repository.removeMovieOfPocket(movieId)
    }

    /**
     * Manage the Actionbar menu
     * 1) onCreateOptionsMenu
     * 2) onOptionsItemSelected
     */

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.information,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let{
            if(it.itemId == R.id.informationButton){
                val intent = Intent(this, InformationActivity::class.java)
                startActivity(intent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * Save the current state (movieID)
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        // save the current movieID
        outState.putLong(MOVIE_ID, movieID)
    }

}