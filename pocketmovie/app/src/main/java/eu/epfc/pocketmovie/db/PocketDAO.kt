package eu.epfc.pocketmovie.db
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import eu.epfc.pocketmovie.model.Movie

@Dao
interface PocketDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addMovieInPocket(movie: Movie):Long

    @Query("SELECT * FROM movies")
    fun getMoviesInPocket():LiveData< List<Movie>>

    @Query("SELECT * FROM movies LIMIT (:page-1)*20,20")
    fun getMoviesInPocketByPage(page:Int):LiveData<List<Movie>>

    @Query("SELECT * FROM movies WHERE id = :movieID")
    fun getMovieByID(movieID:Long):LiveData<Movie>

    @Query("DELETE FROM movies WHERE id = :movieId")
    fun removeMovieFromPocket(movieId: Long):Int
}

