package eu.epfc.pocketmovie.model

class MovieDetail(
    val id: Long,
    val title:String,
    val vote_average:String,
    val poster_path:String,
    val overview:String,
    val releaseDate:String)
