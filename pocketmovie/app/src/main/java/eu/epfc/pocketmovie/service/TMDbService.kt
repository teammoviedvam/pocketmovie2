package eu.epfc.pocketmovie.service



import eu.epfc.pocketmovie.model.Movie
import eu.epfc.pocketmovie.model.MovieDetail
import eu.epfc.pocketmovie.model.VideoDetail
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query






interface TMDbService {

    class ListResult<T>(val results : List<T> ) : List<T> by results

    private fun getApiTmdbKey() = "ea2dcee690e0af8bb04f37aa35b75075"
    private fun getLanguage() = "en-US"

    @GET("/3/movie/popular")
    fun getPopularMoviesByPage
                (@Query("page") page: Int,
                 @Query("api_key") key: String = getApiTmdbKey()
    ): Call<ListResult<Movie>>

    @GET("/3/movie/{id}")
    fun getMovieDetailById
                (@Path("id") id: Long,
                 @Query("api_key") key: String = getApiTmdbKey()
    ): Call<MovieDetail>

    @GET("/3/movie/{id}/videos")
    fun getVideoDetailById
                (@Path("id") id: Long,
                 @Query("api_key") key: String = getApiTmdbKey(),
                 @Query("language") language:String = getLanguage()
    ): Call<ListResult<VideoDetail>>




    companion object {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/")
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
    }
}