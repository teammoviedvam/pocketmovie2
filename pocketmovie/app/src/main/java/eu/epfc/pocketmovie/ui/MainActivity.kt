package eu.epfc.pocketmovie.ui
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import eu.epfc.pocketmovie.R


fun getUrlPoster(poster: String):String = "https://image.tmdb.org/t/p/w500${poster}"

class MainActivity : AppCompatActivity(){

    private lateinit var buttonInformation : ImageButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val adapter = MyFragmentPager(supportFragmentManager)

        adapter.addFragment(TMDbFragment(), "RECENTS MOVIES")
        adapter.addFragment(PocketFragment(), "POCKET")

        val viewPager = findViewById<ViewPager>(R.id.view_pager)
        viewPager.adapter = adapter
        //activity_mainTab.setupWithViewPager(view_pager)
        val tab = findViewById<TabLayout>(R.id.tab_layout)
        tab.setupWithViewPager(viewPager)


    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.information,menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let{
            if(it.itemId == R.id.informationButton){
                val intent = Intent(this, InformationActivity::class.java)
                startActivity(intent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }



    /**
     * La navigation avec des films recents et les pockets
     */
    class MyFragmentPager(manager: FragmentManager): FragmentPagerAdapter(manager){
        val listFragment : MutableList<Fragment> = ArrayList()
        val titleString : MutableList<String> = ArrayList()

        override fun getItem(position: Int): Fragment {
            return listFragment[position]
        }

        override fun getCount(): Int {
            return titleString.size
        }

        fun addFragment(fragment: Fragment, title : String){
            listFragment.add(fragment)
            titleString.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return titleString[position]
        }

    }



}



