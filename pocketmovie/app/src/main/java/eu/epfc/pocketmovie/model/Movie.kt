package eu.epfc.pocketmovie.model

import androidx.room.Entity
import androidx.room.PrimaryKey




@Entity(tableName = "movies")
class Movie(
    @PrimaryKey(autoGenerate = false)
    val id: Long,
    val title:String,
    val vote_average:String,
    val poster_path:String)

