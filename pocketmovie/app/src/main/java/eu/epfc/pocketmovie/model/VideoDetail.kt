package eu.epfc.pocketmovie.model


class VideoDetail(
    val id: String,
    val key:String,
    val site:String,
    val type : String)
